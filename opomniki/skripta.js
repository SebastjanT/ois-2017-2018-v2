window.addEventListener('load', function() {
	//stran nalozena

	//vnos up. imena
	var vnosUP=function(){
		var uporabnik=document.querySelector("#uporabnisko_ime").value;
		document.querySelector("#uporabnik").innerHTML=uporabnik;
		document.querySelector(".pokrivalo").style.visibility="hidden";
	}
	document.querySelector("#prijavniGumb").addEventListener('click', vnosUP);
	
	//novOpomnik
	var novO=function(){
		var nazO=document.querySelector("#naziv_opomnika").value;
		document.querySelector("#naziv_opomnika").value="";
		var casO=document.querySelector("#cas_opomnika").value;
		document.querySelector("#cas_opomnika").value="";
		var opomniki=document.querySelector("#opomniki");
		opomniki.innerHTML+="\
			<div class='opomnik senca rob'>\
				<div class='naziv_opomnika'>"+nazO+"</div>\
				<div class='cas_opomnika'> Opomnik čez <span>"+casO+"</span> sekund.</div>\
			</div>";
	}
	document.querySelector("#dodajGumb").addEventListener('click',novO);
	
	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);
			if (cas==0){
				var nazO = opomnik.querySelector(".naziv_opomnika").innerHTML;
				alert("Opomnik!\n\nZadolžitev '"+nazO+"' je potekla!");
				document.querySelector("#opomniki").removeChild(opomnik);
			}
			else{
				casovnik.innerHTML=cas-1;
			}

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);

});
